#!/bin/bash

shellspec_exec="$1"
test_dir="$2"

$shellspec_exec --reportdir $test_dir/report --tmpdir $(pwd) test_spec.sh
