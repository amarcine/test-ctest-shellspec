project(shine-integration-tests NONE)

cmake_minimum_required(VERSION 3.0)

set(ENV{LC_ALL} C)

find_program(shellspec_exec shellspec)
if(${shellspec_exec} STREQUAL "shellspec_exec-NOTFOUND")
  message(FATAL_ERROR "couldn't find shellspec")
endif()


enable_testing()

file(GLOB_RECURSE tests RELATIVE ${PROJECT_SOURCE_DIR} test_spec.sh)

foreach(test_file ${tests})
  get_filename_component(test ${test_file} DIRECTORY)
  add_test(
    NAME ${test}
    COMMAND ${PROJECT_SOURCE_DIR}/test_runner.sh ${shellspec_exec} ${test}
    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}/${test}"
  )
endforeach()
