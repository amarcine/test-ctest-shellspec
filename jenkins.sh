#!/bin/bash

# git clone https://github.com/AntoniMarcinek/shellspec.git -b gitlab-junit-xml-workaround
git clone https://github.com/shellspec/shellspec.git
export PATH=shellspec:$PATH
export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/latest/Linux-$(uname -m)/bin:$PATH
cmake .
ctest -j $(nproc) || /bin/true
