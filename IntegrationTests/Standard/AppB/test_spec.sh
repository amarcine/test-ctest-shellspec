# Demonstration of a successful test which hides the output of the app from the
# reports.


Describe 'appB'
  It 'produces file with proper content'
    When run ./appB.sh
    The status should be success
    The file 'file.txt' should diff equal 'file.ref.txt'
  End
End
