# Demonstration of a successful test which hides the output of the app from the
# reports.


Describe 'appA'
  It 'runs'
    When run ./appA.sh
    The status should be success
    The output should include "message"
  End
End
