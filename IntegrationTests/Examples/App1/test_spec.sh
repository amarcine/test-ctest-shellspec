# Demonstration of several checks in different contexts


Describe 'app1'
  It 'prints message without args'
    When run ./app1.sh
    The status should be success
    The output should equal 'message'
  End

  It 'fails if given args'
    When run ./app1.sh something
    The status should be failure
  End
End
